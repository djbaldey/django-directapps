��          �      l      �     �     �          (  $   4     Y     l     }     �     �  "   �     �  &   �  .   #  %   R  *   x  $   �  #   �     �     �  >    0   C  (   t  )   �     �  9   �        !   .  5   P     �     �  Z   �  2     O   Q  Z   �  E   �  I   B	  ?   �	  C   �	  /   
     @
                                	         
                                                         Action '%s' not exist. Application is disabled. Application not found. Direct Apps Method '%s' is forbidden for action. Model is disabled. Model not found. Not contains fields for object. Not implemented. Object not found. Please send correct relation name. Relation '%s' not exist. The action name must be in lower case. The request body does not contain the list id. You don't have access to this action. You don't have access to this application. You don't have access to this model. You don't have access to this page. You need to login. hash Project-Id-Version: directapps
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-04-19 17:46+1000
PO-Revision-Date: 2019-04-19 07:42+0000
Last-Translator: Григорий Крамаренко <djbaldey@gmail.com>, 2019
Language-Team: Russian (https://www.transifex.com/rosix/teams/98306/ru/)
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Действия '%s' не существует. Приложение отключено. Приложение не найдено. Direct Apps Метод '%s' запрещён для действия. Модель отключена. Модель не найдена. Не содержит поля для объекта. Не реализовано. Объект не найден. Пожалуйста, отправьте правильное название связи. Отношения '%s' не существует. Имя действия должно быть в нижнем регистре. Тело запроса не содержит списка идентификаторов. Вы не имеете доступа к этому действию. Вы не имеете доступа к этому приложению. Вы не имеете доступа к этой модели. Вы не имеете доступа на эту страницу. Вам нужно авторизоваться. хеш 