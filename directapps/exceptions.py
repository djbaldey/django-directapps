#
# Copyright (c) 2016, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#


class ValidationError(Exception):
    pass


class NotExistError(Exception):
    pass
