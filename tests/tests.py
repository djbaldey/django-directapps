#
# Copyright (c) 2016, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
import os
# import uuid
from decimal import Decimal

from django.apps import apps as django_apps
from django.conf import settings
from django.contrib.auth.models import User, Permission
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.urls import reverse
# from django.utils import timezone
from django.utils.encoding import force_text
# from django.utils.translation import ugettext_lazy as _

from directapps import __version__
from directapps import conf, shortcuts
from directapps.controllers import get_controller

from .models import (
    Section, Category, Photo, Product, Specific
)


BASE_DIR = os.path.dirname(os.path.abspath(__file__))
MANUAL_FILE = os.path.join(BASE_DIR, 'files', 'manual.txt')
PRODUCT_FILE = os.path.join(BASE_DIR, 'files', 'product.jpg')


def get_manual(name='manual'):
    return SimpleUploadedFile(
        name=name + '.txt',
        content=open(MANUAL_FILE, 'rb').read(),
        content_type='text/plain',
    )


def get_photo(name='photo'):
    return SimpleUploadedFile(
        name=name + '.jpg',
        content=open(PRODUCT_FILE, 'rb').read(),
        content_type='image/jpg',
    )


class BaseTestCase(TestCase):

    def setUp(self):
        self.section1 = Section.objects.create(name='Section 1')
        self.product1 = Product.objects.create(
            section=self.section1,
            name='Product 1',
            eancode='1234567890123456',
            price=Decimal('999.99'),
        )
        self.product1.categories.add(
            Category.objects.create(name='Category 1'),
            Category.objects.create(name='Category 2'),
        )
        self.product1.photos.add(
            Photo.objects.create(photo=get_photo()),
            Photo.objects.create(photo=get_photo()),
        )
        Specific.objects.create(product=self.product1, manual=get_manual())
        # Запоминаем пользователей
        self.superuser = User.objects.create_superuser(
            'superuser', 'password', 'root@example.com')
        self.staffuser = User.objects.create_user(
            'staffuser', 'password', 'staff@example.com', is_staff=True)
        self.staffuser.user_permissions.add(*list(Permission.objects.all()))

    def tearDown(self):
        for name in os.listdir(settings.MEDIA_ROOT):
            path = os.path.join(settings.MEDIA_ROOT, name)
            os.remove(path)


class ShortCutsTestCase(BaseTestCase):

    def test_smart_search(self):
        "Test function smart_search()."
        func = shortcuts.smart_search
        qs = Product.objects.all()
        fields = [
            '^name',
            '=name',
            # '@name',  # SQLite not implemented Full-text search
            'name',
        ]
        qs = func(qs, fields, 'PROD UCT 1')
        self.assertEqual(qs.count(), 1)

    def test_shortcuts_get_object_or_none(self):
        "Test function get_object_or_none()."
        func = shortcuts.get_object_or_none
        qs = Category.objects.all()
        category = func(qs, name='Category 1')
        self.assertIsNotNone(category)
        category = func(qs, name='Category 2')
        self.assertIsNotNone(category)
        # DoesNotExist
        category = func(qs, name='Category')
        self.assertIsNone(category)
        # MultipleObjectsReturned
        category = func(qs, name__startswith='Category')
        self.assertIsNone(category)


class URLsTestCase(BaseTestCase):

    def test_version(self):
        # Без авторизации.
        url = reverse('directapps:version')
        data = self.client.get(url).json()
        self.assertEqual(data['directapps'], __version__)
        self.assertEqual(data['checksum'], conf.CHECKSUM_VERSION)

    def test_superuser_apps(self):
        self.client.force_login(self.superuser)
        url = reverse('directapps:apps')
        data = self.client.get(url).json()
        apps = data['apps']
        self.assertIsInstance(apps, list)
        self.assertTrue(apps)
        for a in apps:
            self.assertTrue(a['name'])
            app = django_apps.get_app_config(a['name'])
            self.assertEqual(a['display_name'], force_text(app.verbose_name))
            self.assertEqual(
                a['url'], reverse('directapps:app', args=(app.label,)))
            self.assertFalse(a['complete'])
            for m in a['models']:
                self.assertEqual(m['perms'], 'all')
                self.assertTrue(m['name'])
                model = app.get_model(m['name'])
                meta = model._meta
                self.assertEqual(
                    m['display_name'], force_text(meta.verbose_name_plural))
                self.assertEqual(
                    m['url'],
                    reverse(
                        'directapps:model',
                        args=(meta.app_label, meta.model_name)
                    ),
                )

    def test_get_tests_full_scheme(self):
        self.client.force_login(self.staffuser)
        url = reverse('directapps:app', kwargs={'app': 'tests'})
        a = self.client.get(url).json()
        self.assertIsInstance(a, dict)
        self.assertTrue(a)
        self.assertTrue(a['name'])
        app = django_apps.get_app_config(a['name'])
        self.assertEqual(a['display_name'], force_text(app.verbose_name))
        self.assertEqual(
            a['url'], reverse('directapps:app', args=(app.label,)))
        self.assertTrue(a['complete'])
        for m in a['models']:
            self.assertTrue(m['perms'])
            self.assertTrue(m['name'])
            model = app.get_model(m['name'])
            meta = model._meta
            self.assertEqual(
                m['display_name'], force_text(meta.verbose_name_plural))
            self.assertEqual(
                m['url'],
                reverse(
                    'directapps:model',
                    args=(meta.app_label, meta.model_name)
                ),
            )
            # Из контроллера
            master = get_controller(model)
            model_ctrl = master.model_ctrl
            self.assertTrue(m['filters'])
            self.assertTrue(m['columns'])
            self.assertEqual(
                m['default_ordering'], model_ctrl.default_ordering)
            self.assertEqual(m['order_columns'], model_ctrl.order_columns)
            self.assertEqual(
                m['map_column_relation'], model_ctrl.map_column_relation)
            self.assertEqual(m['columns_key'], model_ctrl.columns_key)
            self.assertEqual(m['ordering_key'], model_ctrl.ordering_key)
            self.assertEqual(
                m['search_key'],
                model_ctrl.search_key if model_ctrl.search_fields else None)
            self.assertEqual(m['limit_key'], model_ctrl.limit_key)
            self.assertEqual(m['page_key'], model_ctrl.page_key)
            self.assertEqual(m['foreign_key'], model_ctrl.foreign_key)
            self.assertEqual(m['limit'], model_ctrl.limit)
            self.assertEqual(m['max_limit'], model_ctrl.max_limit)

    def test_get_users(self):
        self.client.force_login(self.staffuser)
        url = reverse('directapps:model', kwargs={
            'app': 'auth',
            'model': 'user',
            'model_using': 'default',
        })
        data = self.client.get(url).json()
        self.assertEqual(len(data['objects']), 2)
        self.assertEqual(data['page'], 1)
        self.assertEqual(data['num_pages'], 1)
        self.assertEqual(data['info'], None)
        for u in data['objects']:
            self.assertIn('pk', u)
            self.assertIn('fields', u)
            self.assertIn('display_name', u)
            self.assertIn('url', u)
            self.assertNotIn('password', u['fields'])

    def test_add_section(self):
        self.client.force_login(self.staffuser)
        # Создание
        count = Section.objects.count()
        url = reverse('directapps:model', kwargs={
            'app': 'tests',
            'model': 'section',
        })
        data = {
            'name': 'Section Added',
        }
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 200, force_text(res.content))
        self.assertEqual(Section.objects.count(), count + 1)

    def test_change_section(self):
        self.client.force_login(self.staffuser)
        section_name = self.section1.pk
        # Изменение
        url = reverse('directapps:object_action', kwargs={
            'app': 'tests',
            'model': 'section',
            'object': section_name,
            'action': 'update',
        })
        data = {
            'name': 'Section Updated',
        }
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 200, force_text(res.content))
        # Обратное изменение
        url = reverse('directapps:object_action', kwargs={
            'app': 'tests',
            'model': 'section',
            'object': 'Section Updated',
            'action': 'update',
        })
        data = {
            'name': section_name,
        }
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 200, force_text(res.content))

    def test_add_product(self):
        self.client.force_login(self.staffuser)
        # Создание
        count = Product.objects.count()
        url = reverse('directapps:model', kwargs={
            'app': 'tests',
            'model': 'product',
        })
        data = {
            'name': 'Product',
            'section': Section.objects.first().pk,
        }
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 200, force_text(res.content))
        self.assertEqual(Product.objects.count(), count + 1)

    def test_change_product(self):
        self.client.force_login(self.staffuser)
        # Изменение
        url = reverse('directapps:object_action', kwargs={
            'app': 'tests',
            'model': 'product',
            'object': self.product1.pk,
            'action': 'update',
        })
        data = {
            'name': 'Product New Name',
            'section': Section.objects.first().pk,
            'eancode': '000000000',
            'price': Decimal('100.00'),
        }
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 200, force_text(res.content))


class ControllerTestCase(BaseTestCase):

    def test_error_filtering(self):
        self.client.force_login(self.staffuser)
        url = reverse('directapps:model', kwargs={
            'app': 'tests',
            'model': 'product',
        })
        data = {
            'created': 'blabla',
        }
        res = self.client.get(url, data)
        self.assertEqual(res.status_code, 400, force_text(res.content))
