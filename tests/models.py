#
# Copyright (c) 2016, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
import uuid
from decimal import Decimal

# from django.conf import settings
from django.db import models
# from django.utils import timezone
from django.utils.encoding import force_text
from django.utils.translation import ugettext_lazy as _


def upload_to(instance, filename):
    return force_text(filename)


class Section(models.Model):
    "Section of products."

    name = models.CharField(_('name'), max_length=100, primary_key=True)
    parent = models.ForeignKey(
        'Section', verbose_name=_('parent'),
        on_delete=models.CASCADE,
        null=True, blank=True)

    class Meta:
        verbose_name = _('section')
        verbose_name_plural = _('sections')

    def __str__(self):
        return self.name


class Category(models.Model):
    "Category of products."

    name = models.CharField(_('name'), max_length=100)
    description = models.TextField(_('description'), blank=True)

    class Meta:
        verbose_name = _('category')
        verbose_name_plural = _('categories')

    def __str__(self):
        return self.name


class Photo(models.Model):
    "Product photo"

    created = models.DateTimeField(
        _('created'), auto_now_add=True, db_index=True)
    updated = models.DateTimeField(
        _('updated'), auto_now=True, db_index=True)
    photo = models.ImageField(_('photo'), upload_to=upload_to)

    class Meta:
        verbose_name = _('product photo')
        verbose_name_plural = _('product photos')

    def __str__(self):
        return self.photo.url

    def delete(self, **kwargs):
        self.photo.delete()
        super(Photo, self).delete(**kwargs)


class Product(models.Model):
    "Product model."

    created = models.DateTimeField(_('created'), auto_now_add=True)
    updated = models.DateTimeField(_('updated'), auto_now=True)
    section = models.ForeignKey(
        Section, on_delete=models.CASCADE, verbose_name=_('section'))
    uid = models.UUIDField(_('unique ID'), default=uuid.uuid4, blank=True)
    eancode = models.CharField(_('EAN code'), max_length=16, blank=True)
    name = models.CharField(_('name'), max_length=100)
    price = models.DecimalField(
        _('price'), max_digits=10, decimal_places=2, default=Decimal(0),
        blank=True)
    description = models.TextField(_('description'), blank=True)
    categories = models.ManyToManyField(
        Category, verbose_name=_('categories'), blank=True
    )
    photos = models.ManyToManyField(
        Photo, verbose_name=_('photos'), blank=True
    )

    class Meta:
        verbose_name = _('product')
        verbose_name_plural = _('products')

    def __str__(self):
        return self.name


class Specific(models.Model):
    "Product specific."

    COUNTRY_CHOICES = (
        (0, _('unknown')),
        (1, _('Russia')),
        (2, _('China')),
        (3, _('Japan')),
    )
    product = models.OneToOneField(
        Product, on_delete=models.CASCADE, verbose_name=_('product'))
    country = models.IntegerField(_('country'), choices=COUNTRY_CHOICES, default=0)
    producer = models.CharField(_('producer'), max_length=100, blank=True)
    volume = models.FloatField(_('volume'), null=True, blank=True)
    weight = models.FloatField(_('weigth'), null=True, blank=True)
    release_date = models.DateField(_('release date'), null=True, blank=True)
    manual = models.FileField(_('manual'), upload_to=upload_to, blank=True)

    class Meta:
        verbose_name = _('product specific')
        verbose_name_plural = _('product specifics')

    def __str__(self):
        return force_text(self.product)

    def delete(self, **kwargs):
        self.manual.delete()
        super(Specific, self).delete(**kwargs)
