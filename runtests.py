#!/usr/bin/env python3
#
# Copyright (c) 2016, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
import os
import sys

import django
from django.conf import settings
from django.test.utils import get_runner


if __name__ == "__main__":
    os.environ['DJANGO_SETTINGS_MODULE'] = 'tests.test_settings'
    django.setup()
    TestRunner = get_runner(settings)
    test_runner = TestRunner()
    failures = test_runner.run_tests(["tests"])
    dbfile = settings.DATABASES.get('default', {}).get('NAME')
    if dbfile and os.path.exists(dbfile):
        os.remove(dbfile)
    sys.exit(bool(failures))
